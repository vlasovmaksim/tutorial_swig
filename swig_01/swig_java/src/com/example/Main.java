package com.example;

import com.example.generated.*;

import java.lang.reflect.Field;

public class Main {

    // You could load a c++ library here or in the main or any other method.
    static {
        loadLibrary();
    }

    public static void main(String[] args) {
        // Test c++ class that is imported from a dynamic library.
        System.out.println("Creating some objects:");

        Circle c = new Circle(10);

        System.out.println("Created circle");

        Square s = new Square(10);

        System.out.println("Created square");

        c.setX(20);
        c.setY(30);

        System.out.println(c.area());
        System.out.println(c.perimeter());

        s.setX(-10);
        s.setY(5);

        System.out.println("Here is their current position:");
        System.out.format("\nCircle = %f, %f", c.getX(), c.getY());
        System.out.format("\nSquare = %f, %f", s.getX(), s.getY());
    }

    public static void loadLibrary() {
        // Get project directory.
        String projectDir = System.getProperty("user.dir");
        System.out.println("Project directory: " + projectDir);

        // Set library path.
        String libraryDir = projectDir + "/../bin/java";
//        String libraryDir = projectDir + "/../bin/java/RelWithDebInfo";
        System.out.println("Library directory: " + libraryDir);

        // Add library path to the system libraries paths.
        System.setProperty("java.library.path", libraryDir);


        // Links:
//        http://blog.cedarsoft.com/2010/11/setting-java-library-path-programmatically/
//        http://stackoverflow.com/questions/5419039/is-djava-library-path-equivalent-to-system-setpropertyjava-library-path
        Field fieldSysPath = null;
        try {
            fieldSysPath = ClassLoader.class.getDeclaredField("sys_paths");
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }

        assert fieldSysPath != null;
        fieldSysPath.setAccessible(true);

        try {
            fieldSysPath.set(null, null);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        // Print the system libraries paths.
        System.out.println(System.getProperty("java.library.path"));

        // Load the library.
        try {
            System.loadLibrary("example");
        } catch (UnsatisfiedLinkError e) {
            System.err.println("Lbrary failed to load.\n" + e);
            System.exit(1);
        }
    }
}
