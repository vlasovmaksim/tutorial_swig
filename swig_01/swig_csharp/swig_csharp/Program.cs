﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using swig_csharp.generated;


namespace swig_csharp
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Creating some objects:");

            var c = new Circle(10);

            Console.WriteLine("Created circle");

            var s = new Square(10);

            Console.WriteLine("Created square");

            c.x = 20;
            c.y = 30;

            s.x = -10;
            s.y = 5;

            Console.WriteLine("Here is their current position:");
            Console.WriteLine("Circle = ({0}, {1})", c.x, c.y);
            Console.WriteLine("Square = ({0}, {1})", s.x, s.y);
        }
    }
}
