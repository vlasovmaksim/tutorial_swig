# README #

An example of usage C++ code in the C#, Java and Python with help of SWIG and CMake (for Windows only).

C++ project is created with help of CMake.

C++ and Python project is the same as in the [CMake/Tests/SwigTest](https://github.com/Kitware/CMake/tree/master/Tests/SwigTest).

C++ code contains a simple class.

C#, Java, Python projects load C++ dynamic library and call the C++ code.


### The dependencies ###

* [Visual Studio 2015 Community](https://www.visualstudio.com/en-us/products/visual-studio-community-vs.aspx) for C++ and C# project.
* [IntelliJ IDEA](www.jetbrains.com/‎
) for Java project.
* [Python 2.7](https://www.python.org/) for python project.
* [CMake](https://cmake.org/).
* [SWIG](http://www.swig.org/).

### How do I get set up? ###

* Define language as "csharp", "python" or "java" in the *swig_cpp\class\CMakeLists.txt* .
* Run CMake and create C++ win32 Visual Studio 2015 project.
* Build the C++ project. It's better to choose Release build configuration otherwise you will have problem with debug Python libraries.
* If you have choosen the "python" language then simple run the "runme.py" in the "swig_python" folder.
* If you have choosen the "csharp" language then open, build and run the C# project in the "swig_csharp" folder.
* If you have choosen the "java" language then open, build and run the Java project in the "swig_java" folder.